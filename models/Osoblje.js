var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Osoblje', {
		
        ime:{
            type: Sequelize.STRING
        },
        prezime:{
         type: Sequelize.STRING
        },
        uloga:{
        type: Sequelize.STRING
    }
}, {
    tableName: 'Osoblje'
});
};