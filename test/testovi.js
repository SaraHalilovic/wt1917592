let assert = chai.assert;
var kl = document.getElementById("kalendar_prvi");
var mj = new Date().getMonth();
document.getElementById("month-indicator").style.display="none";

function obrisiKalendar() {
    //obrisati prethodni
    const myNode = document.getElementById("kalendar_prvi");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }

}

describe('Kalendar', function () {
    describe('iscrtajKalendar()', function () {
        //Prvi Test
        it('treba imati 30 dana, odnosno 30 elemenata klase date-grid-element za Septembar', function () {


            Kalendar.iscrtajKalendar(kl, 9 - 1);

            let a = document.getElementsByClassName("date-grid-element").item(30) === null
                && document.getElementsByClassName("date-grid-element").item(29) !== null;
            // console.log(document.getElementsByClassName("date-grid-element").item(30)===null);
            //Ako zaista ima 30 dana, broj elemenata u nizu treba biti 30, tj posljednji je na 29 mjestu
            assert.equal(a, true, "Broj dana treba biti 30");


        });

        //Drugi Test
        it('Za decembar treba biti 31 dan, odnosno 31 elemenat klase date-grid-element za Decembar', function () {
            obrisiKalendar();
            Kalendar.iscrtajKalendar(kl, 12 - 1);

            let a = document.getElementsByClassName("date-grid-element").item(31) === null
                && document.getElementsByClassName("date-grid-element").item(30) !== null;
            // console.log(document.getElementsByClassName("date-grid-element").item(30)===null);
            //Ako zaista ima 31 dana, broj elemenata u nizu treba biti 31, tj posljednji je na 30 mjestu u nizu
            assert.equal(a, true, "Broj dana treba biti 30");


        });
        //Treci test, pozvati metodu za trenutni mjesec, prvi element mora biti na 5 mjestu u gridu,prva kolona
        it('pozvati metodu za trenutni mjesec, prvi element mora biti na 5 mjestu u gridu,prvi red', function () {
            obrisiKalendar();
            Kalendar.iscrtajKalendar(kl, mj);
            let peta_celija = document.getElementsByClassName("second-grid").item(0).
                getElementsByClassName("date-grid").item(0).childNodes[4].childNodes[0];
            let datum_u_petoj_celiji = peta_celija.innerHTML;
            assert.equal(datum_u_petoj_celiji, 1, "Datum u 5 celiji u prvom redu treba biti 1");
            let cetvrta_celija = document.getElementsByClassName("date-grid").item(0).childNodes[3];
            let prazan_div = cetvrta_celija.className;
            assert.equal(prazan_div, "prazan_div", "Cetvrta celija u prvom redu treba biti prazan_red")
        });
        //Cetvrti test, pozvati metodu za trenutni mjesec, zadnji dan u subotu
        it('pozvati metodu za trenutni mjesec, zadnji element mora biti na 6 mjestu u gridu,posljednji red', function () {
            obrisiKalendar();
            Kalendar.iscrtajKalendar(kl, mj);
            let sesta_celija = document.getElementsByClassName("second-grid").item(0).
                getElementsByClassName("date-grid").item(0).childNodes[33].childNodes[0];
            let datum_u_sestoj_celiji = sesta_celija.innerHTML;
            assert.equal(datum_u_sestoj_celiji, 30, "Datum u 6 celiji u zadnjem redu treba biti 1");

            let cetvrta_celija = document.getElementsByClassName("date-grid").item(0).childNodes[3];
            let prazan_div = cetvrta_celija.className;
            assert.equal(prazan_div, "prazan_div", "Cetvrta celija u prvom redu treba biti prazan_red")
        });
        
        //Peti test, pozvati metodu za januar, prvi element mora biti u prvom redu kolona 2,a zadnji element u 5 red 4 element 
        it('pozvati metodu za januar, prvi element mora biti u prvom redu kolona 2,a zadnji element u 5 red 4 element', function () {
            obrisiKalendar();
            Kalendar.iscrtajKalendar(kl, 0);
            let prvi = document.getElementsByClassName("second-grid").item(0).
                getElementsByClassName("date-grid").item(0).childNodes[1].childNodes[0];
            let prvi_datum = prvi.innerHTML;
            assert.equal(prvi_datum, 1, "Datum u 2 celiji u prvom redu treba biti 1");

            let posljednji = document.getElementsByClassName("date-grid").item(0).childNodes[31].childNodes[0];
            let zadnji_datum = posljednji.innerHTML;
            assert.equal(zadnji_datum, 31, "Datum u celiji broj 30");

            let cetvrta_celija = document.getElementsByClassName("date-grid").item(0).childNodes[0];
            let prazan_div = cetvrta_celija.className;
            assert.equal(prazan_div, "prazan_div", "Prva celija u prvom redu treba biti prazan_red")

        });

        it('Za februar treba postojati 28 dana, odnosno 28 elemenata klase date-grid-element', function () {
            obrisiKalendar();
            Kalendar.iscrtajKalendar(kl, 2 - 1);

            let a = document.getElementsByClassName("date-grid-element").item(28) === null
                && document.getElementsByClassName("date-grid-element").item(27) !== null;
            assert.equal(a, true, "Broj dana treba biti 28");


        });
        it('Za juli, ponedjeljak je prvi u mjesecu. U prvom redu za juli ne treba postojati niti jedan prazan div (filler)', function () {
            obrisiKalendar();
            Kalendar.iscrtajKalendar(kl,6);

            let a = document.getElementsByClassName("prazan_div").length;
                
            assert.equal(a, 0, "Broj filler divova treba biti 0 (prvi u mjesecu je ponedjeljak)");
            

        });


    })
    
})